package fr.esiea.mongo.main;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.mongodb.util.JSON;
import fr.esiea.mongo.model.Tweet;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (c) 2014 ESIEA M. Labusquiere
 * <p/>
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * <p/>
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * <p/>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
public class MainTest {
    private static final ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring/app-context.xml");
    private static final MongoCollection jongoCollection = applicationContext.getBean(MongoCollection.class);
    private static final DBCollection collection = applicationContext.getBean(DBCollection.class);


    public static void main(String[] args) {
        Tweet tweet = new Tweet();

        ObjectId id = new ObjectId();
        tweet.set_id(id);
        tweet.setText("text");

        //We fill the databse with an object and keep a reference to the id
        // the id is ObjectId("...") in the database => comportement normale de mongodb quand il génère les id
        jongoCollection.insert(tweet);

        //Java Driver
        //Creation of two query

        DBObject query1 = new BasicDBObject("_id",id);

        DBObject bsonObjectIdRepresentation = new BasicDBObject("$oid",id.toString());
        DBObject query2 = new BasicDBObject("_id",bsonObjectIdRepresentation);

        String serializedQuery1 = JSON.serialize(query1);
        String serializedQuery2 = JSON.serialize(query2);

        if(serializedQuery1.equals(serializedQuery2))
            System.out.println("Query1 et Query2 sont serialisé par mongo de la même facon " + serializedQuery1);

        //Call the database with query1 and MJD
        DBObject firstResult = collection.findOne(query1);
        System.out.println(firstResult);

        //Call the database with query2 and MJD
        try {
            System.out.println(collection.findOne(query2));
        } catch (MongoException e) {
            System.out.println("Query2 not working ");
            e.printStackTrace();
        }

        //Call the database with jongo and the serialisation of queries
        System.out.println(jongoCollection.findOne(serializedQuery1).as(Tweet.class) + " Be aware it's the toString of Tweet and marshaller by jackson");

        //Dumb verification
        if(serializedQuery1.contains(JSON.serialize(bsonObjectIdRepresentation)))
            System.out.println("Dumb verification done");
    }
}
        /**
         * Notes
         * Mongo shell

         > db.tweets.find({"_id": {"$oid":"52f4e2799a69666738c88fd2"}},{_id:1})

         error: { "$err" : "invalid operator: $oid", "code" : 10068 }

          

         > db.tweets.find( {"_id" : "52f4e2799a69666738c88fd2"} , {_id:1} )

         pas de resultat

          

         > db.tweets.find( {"_id" : ObjectId("52f4e2799a69666738c88fd2")} , {_id:1} )

         { "_id" : ObjectId("52f4e2799a69666738c88fd2") } => resultat

          

         bien entendu

         > ObjectId

         function ObjectId() {

             [native code]

         }

          

         Javaside avec Jongo  => org.jongo.MongoCollection

          

         String id= "52f4e2799a69666738c88fd2"

         mongoCollection.findOne(org.jongo.Oid.withOid(id)).as(Tweet.class);

          

         retourne l'objet

          

         mais en passant par le driver :

          

                 DBObject oid = BasicDBObjectBuilder.start("$oid", id).get();

                 DBObject dbObject = BasicDBObjectBuilder.start("_id", oid).get();

                 DBObject object = collection.findOne(dbObject);

          

         une exception est levé :

          

         Exception in thread "main" com.mongodb.MongoException: invalid operator: $oid

                       at com.mongodb.MongoException.parse(MongoException.java:82)

                       at com.mongodb.DBApiLayer$MyCollection.__find(DBApiLayer.java:292)

                       at com.mongodb.DBApiLayer$MyCollection.__find(DBApiLayer.java:273)

                       at com.mongodb.DBCollection.findOne(DBCollection.java:728)

                       at com.mongodb.DBCollection.findOne(DBCollection.java:670)

                       at fr.zenika.performance.dao.mjd.MJDao.findById(MJDao.java:112)

                       at fr.zenika.performance.MainPerfTest.main(MainPerfTest.java:78)

         Disconnected from the target VM, address: '127.0.0.1:51713', transport: 'socket'

          

         par contre ce code marche

          

                 DBObject dbObject = BasicDBObjectBuilder.start("_id",new ObjectId(id)).get();

                 DBObject object = collection.findOne(dbObject);

          

         Notes :

         Au debugger lors de l'appel jongo qui marche on a dans la class findOne ligne 51 (appelé par le

         as())

         le champ result (implementation bson.RelaxedLazyObject) (ligne 51) contient bien {"_id":

         {"$oid":"52f4e2799a69666738c88fd2"}, …....

          

         pour rappel :

            MongoCollection.MONGO_QUERY_OID = "$oid";

         et

             public static String withOid(String id) {

                 return "{_id: {" + MONGO_QUERY_OID + ":\"" + id + "\"}}";

             }
         */

